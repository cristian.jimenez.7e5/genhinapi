package com.example.genshinapi.database

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "CharacterEntity")
data class CharacterEntity(
    @PrimaryKey(autoGenerate = true) var id: Long = 0,
    val description: String,
    val name: String,
    val nation: String,
    val rarity: Int,
    val vision: String,
    val weapon: String
)