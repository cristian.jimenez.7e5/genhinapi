package com.example.genshinapi.database

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.example.genshinapi.model.CharacterData

@Dao
interface CharacterDAO {
    @Query("SELECT * FROM CharacterEntity")
    fun getAllCharacters(): MutableList<CharacterData>

    @Query("SELECT * FROM CharacterEntity WHERE name = :characterName")
    fun getCharacterByName(characterName: String): MutableList<CharacterEntity>

//    @Query("SELECT * FROM CharacterEntity WHERE favorite")
//    fun getFavoriteCharacters(): MutableList<CharacterEntity>

    @Insert
    fun addCharacter(characterEntity: CharacterEntity)

    @Update
    fun updateCharacter(characterEntity: CharacterEntity)

    @Delete
    fun deleteCharacter(characterEntity: CharacterEntity)


    // He hagut de crear una query per esborrar un character ja que la funcio
    // de deleteCharacter() no funciona xd

    @Query("DELETE FROM CharacterEntity WHERE name = :characterName")
    fun deleteThisCharacter(characterName: String)
}