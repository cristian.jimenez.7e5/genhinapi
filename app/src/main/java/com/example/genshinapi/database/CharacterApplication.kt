package com.example.genshinapi.database

import android.app.Application
import androidx.room.Room

class CharacterApplication: Application() {
    companion object {
        lateinit var database: CharacterDatabase
    }

    override fun onCreate() {
        super.onCreate()
        database = Room.databaseBuilder(this,
            CharacterDatabase::class.java,
            "CharacterDatabase").build()
    }
}

// Això en veritat no seria "CharacterApplication" ja que si volgues fer el mateix per artifacts
// no em caldria fer servir un altre document, sino que seria aquest mateix, el que hauria de
// fer es un nou "ArtifactEntity"