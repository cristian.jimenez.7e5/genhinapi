package com.example.genshinapi.retrofit

import com.example.genshinapi.model.ArtifactsData
import com.example.genshinapi.model.ArtifactsName
import com.example.genshinapi.model.CharacterData
import com.example.genshinapi.model.CharactersName
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Url

interface ApiInterface {
    @GET
    suspend fun getCharactersNamesData(@Url url: String): Response<CharactersName>

    @GET
    suspend fun getSpecificCharacterData(@Url url: String): Response<CharacterData>


    @GET
    suspend fun getArtifactsNamesData(@Url url: String): Response<ArtifactsName>

    @GET
    suspend fun getSpecificArtifactData(@Url url: String): Response<ArtifactsData>


    companion object {
        val BASE_URL = "https://api.genshin.dev/"
        fun create(): ApiInterface {
            val client = OkHttpClient.Builder().build()
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()
            return retrofit.create(ApiInterface::class.java)
        }
    }
}
