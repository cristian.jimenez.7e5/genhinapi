package com.example.genshinapi

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.genshinapi.databinding.ActivityMainBinding
import com.example.genshinapi.fragment.MenuFragment

class MainActivity: AppCompatActivity() {

    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragmentContainerView, MenuFragment())
            setReorderingAllowed(true)
            addToBackStack("name") // name can be null
            commit()
        }
    }
}