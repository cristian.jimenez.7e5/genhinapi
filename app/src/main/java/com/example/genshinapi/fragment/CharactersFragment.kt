package com.example.genshinapi.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.genshinapi.OnClickListener
import com.example.genshinapi.R
import com.example.genshinapi.adapter.CharactersAdapter
import com.example.genshinapi.database.CharacterApplication
import com.example.genshinapi.databinding.FragmentCharactersBinding
import com.example.genshinapi.model.CharactersName
import com.example.genshinapi.viewModel.CharactersViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class CharactersFragment(val dataBeingDisplayed: String = "fullView") : Fragment(), OnClickListener, SearchView.OnQueryTextListener,
    androidx.appcompat.widget.SearchView.OnQueryTextListener {

    lateinit var binding: FragmentCharactersBinding
    private lateinit var charactersAdapter: CharactersAdapter
    private lateinit var gridLayoutManager: RecyclerView.LayoutManager
    private val viewModel: CharactersViewModel by activityViewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentCharactersBinding.inflate(inflater, container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        gridLayoutManager = GridLayoutManager(context, 2)
        viewModel.charactersNames.observe(viewLifecycleOwner) {
            setUpRecyclerView(it)
        }

        binding.searchTextV2.setOnQueryTextListener(this)

        binding.favoritesIcon.setOnClickListener {
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, CharactersFragment("favoriteView"))
                setReorderingAllowed(true)
                addToBackStack("name") // name can be null
                commit()
            }
        }
        binding.searchIcon.setOnClickListener {

            if(binding.searchBox.visibility == View.GONE){
                parentFragmentManager.beginTransaction().apply {
                    replace(R.id.fragmentContainerView, CharactersFragment("searchView"))
                    setReorderingAllowed(true)
                    addToBackStack("name") // name can be null
                    commit()
                }
            } else{
                parentFragmentManager.beginTransaction().apply {
                    replace(R.id.fragmentContainerView, CharactersFragment("fullView"))
                    setReorderingAllowed(true)
                    addToBackStack("name") // name can be null
                    commit()
                }
            }
        }
        binding.title.setOnClickListener {
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, MenuFragment())
                setReorderingAllowed(true)
                addToBackStack("name") // name can be null
                commit()
            }
        }
    }

    private fun setUpRecyclerView(myData: CharactersName) {

        when (dataBeingDisplayed) {
            "fullView" -> {
                val upperCaseData = CharactersName()
                myData.forEach {
                    upperCaseData.add(it.uppercase())
                }
                charactersAdapter = CharactersAdapter(upperCaseData, this)
            }
            "favoriteView" -> {
                val charactersNamesList = CharactersName()
                CoroutineScope(Dispatchers.Main).launch {
                    val charactersList = withContext(Dispatchers.IO) { CharacterApplication.database.characterDao().getAllCharacters() }
                    charactersList.forEach {
                        charactersNamesList.add(it.name.uppercase())
                    }
                    charactersAdapter.setCharacterList(charactersNamesList)
                }
                charactersAdapter = CharactersAdapter(charactersNamesList, this)
            }
            "searchView" -> {
                binding.searchBox.visibility = View.VISIBLE

                val upperCaseData = CharactersName()
                myData.forEach {
                    upperCaseData.add(it.uppercase())
                }
                charactersAdapter = CharactersAdapter(upperCaseData, this)
            }
        }

        binding.recycler.apply {
            setHasFixedSize(true) //Optimitza el rendiment de l’app
            layoutManager = gridLayoutManager
            adapter = charactersAdapter
        }
    }

    override fun onClick(character: String) {
        viewModel.setCharacter(character)
        parentFragmentManager.beginTransaction().apply {
            replace(R.id.fragmentContainerView, DetailedCharacterFragment())
            setReorderingAllowed(true)
            addToBackStack("name") // name can be null
            commit()
        }
    }

    override fun onQueryTextSubmit(p0: String?): Boolean {
        return false
    }

    override fun onQueryTextChange(p0: String?): Boolean {
        val filteredList = viewModel.charactersNames.value?.filter { it.contains(p0.toString(), true) }
        val adaptedList = CharactersName()
        filteredList?.forEach {
            adaptedList.add(it.toUpperCase())
        }
        binding.recycler.adapter = CharactersAdapter(adaptedList, this)
        return true
    }
}