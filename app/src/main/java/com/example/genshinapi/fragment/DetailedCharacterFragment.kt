package com.example.genshinapi.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.genshinapi.databinding.FragmentDetailedCharactersBinding
import com.example.genshinapi.viewModel.CharactersViewModel
import androidx.fragment.app.activityViewModels
import com.example.genshinapi.R
import com.example.genshinapi.database.CharacterApplication
import com.example.genshinapi.database.CharacterEntity
import com.example.genshinapi.model.CharacterData
import com.squareup.picasso.Picasso
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class DetailedCharacterFragment: Fragment() {

    lateinit var binding: FragmentDetailedCharactersBinding
    private val viewModel: CharactersViewModel by activityViewModels()
    lateinit var currentCharacterEntity: CharacterData
    var isCharacterFavorite: Boolean = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentDetailedCharactersBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?){
        super.onViewCreated(view, savedInstanceState)
        val currentCharacter = viewModel.currentCharacter.value?.lowercase()?.split(" ")?.joinToString(" ") { it.capitalize() }

        CoroutineScope(Dispatchers.IO).launch {
            if (currentCharacter != null) {
                val didFindCharacters = withContext(Dispatchers.IO) { CharacterApplication.database.characterDao().getCharacterByName(currentCharacter) }
                isCharacterFavorite = didFindCharacters.isNotEmpty()
            }
        }

        if (currentCharacter != null) {
            viewModel.fetchSpecificCharacterData("characters/$currentCharacter")
        }
        viewModel.charactersData.observe(viewLifecycleOwner) {
            currentCharacterEntity = it
            binding.characterNameValue.text = it.name
            binding.characterNationValue.text = it.nation
            binding.characterVisionValue.text = it.vision
            binding.characterWeaponValue.text = it.weapon
            binding.characterDescription.text = it.description
            if(it.rarity == 4){
                binding.characterRarityValue.text = "⭐ ⭐ ⭐ ⭐ "
            } else{
                binding.characterRarityValue.text = "⭐ ⭐ ⭐ ⭐ ⭐ "
            }
            loadCharacterCard(currentCharacter, binding)
            binding.heartButton.isChecked = isCharacterFavorite
        }
        binding.heartButton.setOnClickListener {
            val characterEntity = CharacterEntity(
                description = currentCharacterEntity.description,
                name = currentCharacterEntity.name,
                nation = currentCharacterEntity.nation,
                rarity = currentCharacterEntity.rarity,
                vision = currentCharacterEntity.vision,
                weapon = currentCharacterEntity.weapon,
            )
            CoroutineScope(Dispatchers.Main).launch {
                withContext(Dispatchers.IO) {
                    if(isCharacterFavorite){
                        CharacterApplication.database.characterDao().deleteThisCharacter(characterEntity.name)
                    } else{
                        CharacterApplication.database.characterDao().addCharacter(characterEntity) }
                    }
                    isCharacterFavorite = !isCharacterFavorite
            }
        }
        binding.favoritesIcon.setOnClickListener {
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, CharactersFragment("favoriteView"))
                setReorderingAllowed(true)
                addToBackStack("name") // name can be null
                commit()
            }
        }
        binding.searchIcon.setOnClickListener {
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, CharactersFragment("searchView"))
                setReorderingAllowed(true)
                addToBackStack("name") // name can be null
                commit()
            }
        }
        binding.title.setOnClickListener {
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, MenuFragment())
                setReorderingAllowed(true)
                addToBackStack("name") // name can be null
                commit()
            }
        }
    }

    private fun loadCharacterCard(character: String?, binding: FragmentDetailedCharactersBinding) {
        Picasso.get()
            .load("https://api.genshin.dev/characters/${character?.lowercase()}/gacha-splash.png")
            .placeholder(R.drawable.logo_genshin_api_removed_background)
            .error(R.drawable.logo_genshin_api_removed_background)
            .into(binding.characterCard)
    }
}