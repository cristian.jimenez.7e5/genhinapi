package com.example.genshinapi.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.genshinapi.R
import com.example.genshinapi.databinding.FragmentInformationBinding

class InformationFragment: Fragment() {
    lateinit var binding: FragmentInformationBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentInformationBinding.inflate(inflater, container,false)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.title.setOnClickListener {
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, MenuFragment())
                setReorderingAllowed(true)
                addToBackStack("name") // name can be null
                commit()
            }
        }

        binding.informationMessage.text = """
            Welcome to GenshinApi!
                
            This is an android studio project coded with Kotlin (and a lot of hope), with the main theme being the game Genshin Impact.
            
            For the moment, this app is only full functionally for Characters.
            
            These are some functionalities for the app:
            
                * If you click on the title, it'll lead you to home page.
            
                * If you press Characters > Albedo (f.e.) > Click on the heart: you will have added Albedo to your favorites characters, and that list is able to see when you click on the "favorite button" at the bottom of the app.
                
                * If you click on the magnifying glass, a text box will appear and you can type what character are you interested in. If you click on it again, the text box will disappear.
                
                * Detailed view of the character is also possible to access through the favorite view.
                  
            Gotta say that there's some errors coming from the api itself which I can do nothing to, like missing images.
            
            (Press on title to go back)
        """.trimIndent()
    }
}