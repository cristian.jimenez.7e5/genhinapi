package com.example.genshinapi.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.genshinapi.OnClickListener
import com.example.genshinapi.R
import com.example.genshinapi.adapter.ArtifactsAdapter
import com.example.genshinapi.database.CharacterEntity
import com.example.genshinapi.databinding.FragmentArtifactsBinding
import com.example.genshinapi.model.ArtifactsName
import com.example.genshinapi.viewModel.ArtifactsViewModel

class ArtifactsFragment : Fragment(), OnClickListener {

    lateinit var binding: FragmentArtifactsBinding
    private lateinit var artifactsAdapter: ArtifactsAdapter
    private lateinit var gridLayoutManager: RecyclerView.LayoutManager
    private val viewModel: ArtifactsViewModel by activityViewModels()

   override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
       binding = FragmentArtifactsBinding.inflate(inflater, container,false)
       return binding.root
   }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        gridLayoutManager = GridLayoutManager(context, 2)
        viewModel.artifactsNames.observe(viewLifecycleOwner){
            setUpRecyclerView(it)
        }
        binding.title.setOnClickListener {
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, MenuFragment())
                setReorderingAllowed(true)
                addToBackStack("name") // name can be null
                commit()
            }
        }
    }

    private fun setUpRecyclerView(myData: ArtifactsName) {
        val upperCaseData = ArtifactsName()
        myData.forEach {
            upperCaseData.add(it.uppercase())
        }
        artifactsAdapter = ArtifactsAdapter(upperCaseData, this)
        binding.recycler.apply {
            setHasFixedSize(true) //Optimitza el rendiment de l’app
            layoutManager = gridLayoutManager
            adapter = artifactsAdapter
        }
    }

    override fun onClick(artifact: String) {
        viewModel.setArtifact(artifact)
        parentFragmentManager.beginTransaction().apply {
            replace(R.id.fragmentContainerView, DetailedArtifactFragment())
            setReorderingAllowed(true)
            addToBackStack("name") // name can be null
            commit()
        }
    }
}