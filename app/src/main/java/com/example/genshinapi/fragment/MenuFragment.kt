package com.example.genshinapi.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.genshinapi.R
import com.example.genshinapi.databinding.FragmentMenuBinding


class MenuFragment : Fragment() {

    lateinit var binding: FragmentMenuBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMenuBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.artifactsConstraintLayout.setOnClickListener {
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, ArtifactsFragment())
                setReorderingAllowed(true)
                addToBackStack("name") // name can be null
                commit()
            }
        }

        binding.artifactImage.setOnClickListener {
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, ArtifactsFragment())
                setReorderingAllowed(true)
                addToBackStack("name") // name can be null
                commit()
            }
        }

        binding.characterConstraintLayout.setOnClickListener {
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, CharactersFragment())
                setReorderingAllowed(true)
                addToBackStack("name") // name can be null
                commit()
            }
        }

        binding.artifactImage.setOnClickListener {
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, CharactersFragment())
                setReorderingAllowed(true)
                addToBackStack("name") // name can be null
                commit()
            }
        }

        binding.weaponConstraintLayout.setOnClickListener {
            Toast.makeText(this.context, "This feature is still under construction.", Toast.LENGTH_SHORT).show()
        }

        binding.weaponImage.setOnClickListener {
            Toast.makeText(this.context, "This feature is still under construction.", Toast.LENGTH_SHORT).show()
        }


        binding.nationConstraintLayout.setOnClickListener {
            Toast.makeText(this.context, "This feature is still under construction.", Toast.LENGTH_SHORT).show()
        }


        binding.nationImage.setOnClickListener {
            Toast.makeText(this.context, "This feature is still under construction.", Toast.LENGTH_SHORT).show()
        }


        binding.enemiesConstraintLayout.setOnClickListener {
            Toast.makeText(this.context, "This feature is still under construction.", Toast.LENGTH_SHORT).show()
        }

        binding.enemyImage.setOnClickListener {
            Toast.makeText(this.context, "This feature is still under construction.", Toast.LENGTH_SHORT).show()
        }

        binding.materialsConstraintLayout.setOnClickListener {
            Toast.makeText(this.context, "This feature is still under construction.", Toast.LENGTH_SHORT).show()
        }

        binding.materialImage.setOnClickListener {
            Toast.makeText(this.context, "This feature is still under construction.", Toast.LENGTH_SHORT).show()
        }

        binding.informationButton.setOnClickListener {
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, InformationFragment())
                setReorderingAllowed(true)
                addToBackStack("name") // name can be null
                commit()
            }
        }

    }
}