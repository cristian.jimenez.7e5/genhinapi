package com.example.genshinapi.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.example.genshinapi.R
import com.example.genshinapi.databinding.FragmentDetailedArtifactsBinding
import com.example.genshinapi.viewModel.ArtifactsViewModel
import com.squareup.picasso.Picasso

class DetailedArtifactFragment: Fragment() {

    lateinit var binding: FragmentDetailedArtifactsBinding
    private val viewModel: ArtifactsViewModel by activityViewModels()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentDetailedArtifactsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?){
        super.onViewCreated(view, savedInstanceState)
        val currentArtifact = viewModel.currentArtifact.value?.lowercase()
        if (currentArtifact != null) {
            viewModel.fetchSpecificArtifactData("artifacts/$currentArtifact")
        }
        viewModel.artifactsData.observe(viewLifecycleOwner) {
            binding.artifactName.text = it.name
            binding.twoPieceBonusValue.text = it.`2-piece_bonus`
            binding.fourPieceBonusValue.text = it.`4-piece_bonus`
            if(it.max_rarity == 3){
                binding.characterRarityValue.text = "⭐ ⭐ ⭐ "
            } else if(it.max_rarity == 4){
                binding.characterRarityValue.text = "⭐ ⭐ ⭐ ⭐ "
            } else{
                binding.characterRarityValue.text = "⭐ ⭐ ⭐ ⭐ ⭐ "
            }
            loadArtifactImage(currentArtifact, binding)
        }
        binding.title.setOnClickListener {
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, MenuFragment())
                setReorderingAllowed(true)
                addToBackStack("name") // name can be null
                commit()
            }
        }
    }

    private fun loadArtifactImage(artifact: String?, binding: FragmentDetailedArtifactsBinding) {
        Picasso.get()
            .load("https://api.genshin.dev/artifacts/${artifact?.lowercase()}/flower-of-life.png")
            .placeholder(R.drawable.logo_genshin_api_removed_background)
            .error(R.drawable.logo_genshin_api_removed_background)
            .into(binding.artifactImage)
    }

}