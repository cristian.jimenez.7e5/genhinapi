package com.example.genshinapi.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.genshinapi.Repository
import com.example.genshinapi.model.CharacterData
import com.example.genshinapi.model.CharactersName
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class CharactersViewModel: ViewModel(){
    val repository = Repository()
    var currentCharacter = MutableLiveData<String>()
    var charactersNames = MutableLiveData<CharactersName>()
    var charactersData = MutableLiveData<CharacterData>()

    init {
        fetchCharactersNamesData("characters")
    }

    fun fetchCharactersNamesData(url: String){
        viewModelScope.launch {
            val charactersNamesData = withContext(Dispatchers.IO) { repository.fetchCharactersNamesData(url) }
            charactersNames.postValue(charactersNamesData)
        }
    }

    fun fetchSpecificCharacterData(url: String){
        viewModelScope.launch {
            val specificCharacterData = withContext(Dispatchers.IO) { repository.fetchSpecificCharacterData(url) }
            charactersData.postValue(specificCharacterData)
        }
    }

    fun setCharacter(character: String){
        currentCharacter.value = character
    }
}