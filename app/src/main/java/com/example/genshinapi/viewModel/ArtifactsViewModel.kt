package com.example.genshinapi.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.genshinapi.Repository
import com.example.genshinapi.model.ArtifactsData
import com.example.genshinapi.model.ArtifactsName
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ArtifactsViewModel: ViewModel(){
    val repository = Repository()
    var currentArtifact = MutableLiveData<String>()
    var artifactsNames = MutableLiveData<ArtifactsName>()
    var artifactsData = MutableLiveData<ArtifactsData>()

    init {
        fetchArtifactsNamesData("artifacts")
    }

    fun fetchArtifactsNamesData(url: String){
        viewModelScope.launch {
            val artifactsNamesData = withContext(Dispatchers.IO) { repository.fetchArtifactsNamesData(url) }
            artifactsNames.postValue(artifactsNamesData)
        }
    }

    fun fetchSpecificArtifactData(url: String){
        viewModelScope.launch {
            val specificArtifactData = withContext(Dispatchers.IO) { repository.fetchSpecificArtifactData(url) }
            artifactsData.postValue(specificArtifactData)
        }
    }

    fun setArtifact(artifact: String) {
        currentArtifact.value = artifact
    }
}