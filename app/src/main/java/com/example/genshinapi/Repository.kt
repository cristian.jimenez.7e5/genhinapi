package com.example.genshinapi

import com.example.genshinapi.model.ArtifactsData
import com.example.genshinapi.model.ArtifactsName
import com.example.genshinapi.model.CharacterData
import com.example.genshinapi.model.CharactersName
import com.example.genshinapi.retrofit.ApiInterface

class Repository {

    private val apiInterface = ApiInterface.create()

    suspend fun fetchCharactersNamesData(url: String): CharactersName?{
        val response = apiInterface.getCharactersNamesData(url)
        if(response.isSuccessful){
            return response.body()!!
        } else{
            return null
        }
    }

    suspend fun fetchSpecificCharacterData(url: String): CharacterData?{
        val response = apiInterface.getSpecificCharacterData(url)
        if(response.isSuccessful){
            return response.body()!!
        } else{
            return null
        }
    }


    suspend fun fetchArtifactsNamesData(url: String): ArtifactsName?{
        val response = apiInterface.getArtifactsNamesData(url)
        if(response.isSuccessful){
            return response.body()!!
        } else{
            return null
        }
    }

    suspend fun fetchSpecificArtifactData(url: String): ArtifactsData?{
        val response = apiInterface.getSpecificArtifactData(url)
        if(response.isSuccessful){
            return response.body()!!
        } else{
            return null
        }
    }


}
