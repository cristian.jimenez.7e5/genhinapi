package com.example.genshinapi


interface OnClickListener {
    fun onClick(element: String)
}