package com.example.genshinapi.model

data class ArtifactsData(
    val `2-piece_bonus`: String,
    val `4-piece_bonus`: String,
    val max_rarity: Int,
    val name: String
)