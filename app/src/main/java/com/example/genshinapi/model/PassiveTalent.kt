package com.example.genshinapi.model

data class PassiveTalent(
    val description: String,
    val level: Int,
    val name: String,
    val unlock: String
)