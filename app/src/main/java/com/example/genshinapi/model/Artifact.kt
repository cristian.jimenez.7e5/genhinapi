package com.example.genshinapi.model

class Artifact(val name: String, val twoArtifactsStat: String, val fourArtifactsStat: String)