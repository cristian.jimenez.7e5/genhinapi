package com.example.genshinapi.model

data class Constellation(
    val description: String,
    val level: Int,
    val name: String,
    val unlock: String
)