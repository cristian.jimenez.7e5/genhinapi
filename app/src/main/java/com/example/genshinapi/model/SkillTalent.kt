package com.example.genshinapi.model

import com.google.gson.annotations.SerializedName

data class SkillTalent(
    @SerializedName("mis huevos")
    val description: String,
    val name: String,
    val type: String,
    val unlock: String
)