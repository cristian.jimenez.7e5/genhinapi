package com.example.genshinapi

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen

class LaunchScreen : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        val splashScreen = installSplashScreen()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_launch_screen)
        splashScreen.setKeepOnScreenCondition{ true }
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }
}