package com.example.genshinapi.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.genshinapi.OnClickListener
import com.example.genshinapi.R
import com.example.genshinapi.databinding.ArtifactLayoutBinding
import com.example.genshinapi.model.ArtifactsName
import com.squareup.picasso.Picasso

class ArtifactsAdapter(
    private val artifacts: ArtifactsName,
    private val listener: OnClickListener
): RecyclerView.Adapter<ArtifactsAdapter.ViewHolder>() {

    private lateinit var context: Context

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val binding = ArtifactLayoutBinding.bind(view)
        fun setListener(artifact: String){
            binding.root.setOnClickListener {
                listener.onClick(artifact)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(parent.context).inflate(R.layout.artifact_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val artifact = artifacts[position]
        with(holder){
            setListener(artifacts[position])
            binding.artifactName.text = artifact
            loadArtifactImage(artifact, binding)
        }
    }

    private fun loadArtifactImage(artifact: String, binding: ArtifactLayoutBinding) {
        Picasso.get()
            .load("https://api.genshin.dev/artifacts/${artifact.lowercase()}/flower-of-life.png")
            .placeholder(R.drawable.logo_genshin_api_removed_background)
            .error(R.drawable.logo_genshin_api_removed_background)
            .into(binding.artifactImage)
    }

    override fun getItemCount(): Int {
        return artifacts.size
    }
}