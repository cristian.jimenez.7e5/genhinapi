package com.example.genshinapi.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.genshinapi.OnClickListener
import com.example.genshinapi.R
import com.example.genshinapi.database.CharacterApplication
import com.example.genshinapi.database.CharacterEntity
import com.example.genshinapi.databinding.CharacterLayoutBinding
import com.example.genshinapi.model.CharacterData
import com.example.genshinapi.model.CharactersName
import com.squareup.picasso.Picasso
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class CharactersAdapter(
    private var characters: CharactersName,
    private val listener: OnClickListener
): RecyclerView.Adapter<CharactersAdapter.ViewHolder>() {

    private lateinit var context: Context

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val binding = CharacterLayoutBinding.bind(view)
        fun setListener(character: String){
            binding.root.setOnClickListener{
                listener.onClick(character)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(parent.context).inflate(R.layout.character_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val character = characters[position]
        with(holder){
            setListener(characters[position])
            binding.characterName.text = character
//            binding.heartButton.isChecked = character
            loadCharacterCard(character, binding)
        }
    }

    private fun loadCharacterCard(character: String, binding: CharacterLayoutBinding) {
        Picasso.get()
            .load("https://api.genshin.dev/characters/${character.lowercase()}/card.jpg")
            .placeholder(R.drawable.logo_genshin_api_removed_background)
            .error(R.drawable.logo_genshin_api_removed_background)
            .into(binding.characterCard)
    }

    override fun getItemCount(): Int {
        return characters.size
    }

    fun setCharacterList(characterList: CharactersName){
        this.characters = characterList
        notifyDataSetChanged()
    }
}